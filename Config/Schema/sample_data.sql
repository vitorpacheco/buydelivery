-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 04/03/2012 às 17h06min
-- Versão do Servidor: 5.1.58
-- Versão do PHP: 5.3.6-13ubuntu3.6

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `buy_delivery`
--

--
-- Extraindo dados da tabela `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `created`, `status`, `total`) VALUES
(8, 2, '2012-03-04 01:48:24', 1, 79.97);

--
-- Extraindo dados da tabela `orders_plates`
--

INSERT INTO `orders_plates` (`id`, `order_id`, `plate_id`, `quantity`) VALUES
(5, 8, 1, 2),
(12, 8, 2, 1);

--
-- Extraindo dados da tabela `plates`
--

INSERT INTO `plates` (`id`, `active`, `restaurant_id`, `title`, `description`, `price`) VALUES
(1, 1, 1, 'Pizza Grande de Frango com Catupiry', 'Peito de frango desfiado, queijo catupiry, milho verde, molho de tomate apurado, orégano e azeitonas.', 29.99),
(2, 1, 1, 'Pizza Média de Frango com Catupiry', 'Peito de frango desfiado, queijo catupiry, milho verde, molho de tomate apurado, orégano e azeitonas.', 19.99),
(5, 1, 1, 'Pizza Média de Chocolate', 'Chocolate ao leite, coco ralado, cereja em calda, doce de leite e queijo muzzarela.', 19.99);

--
-- Extraindo dados da tabela `restaurants`
--

INSERT INTO `restaurants` (`id`, `active`, `user_id`, `name`) VALUES
(1, 1, 1, 'Pizzaria da Esquina'),
(2, 1, 5, 'Cantina Cortile'),
(3, 1, 5, 'China In Box'),
(4, 1, 27, 'Dragão Chinês'),
(5, 1, 27, 'Lig-Lig Restaurante e Delivery'),
(6, 1, 30, 'Matsuri'),
(7, 1, 63, 'Frango Legal'),
(8, 1, 42, 'Casarão das Pizzas'),
(9, 1, 5, 'Cheiro de Pizza'),
(10, 1, 83, 'Colombo Pizzaria do Casarão'),
(11, 1, 74, 'Mustaffá'),
(12, 1, 54, 'Me Gusta Sabor Mexicano'),
(13, 1, 44, 'Sussa Forneria'),
(14, 1, 32, 'Santo Tempero'),
(15, 1, 20, 'Porto Bardauê'),
(16, 1, 1, 'Tijuana'),
(17, 1, 35, 'Volare'),
(18, 1, 66, 'Cantina Volpi'),
(19, 1, 47, 'Beach Stop'),
(21, 1, 63, 'Fratello Pizzeria'),
(22, 1, 31, 'Odoyá'),
(23, 1, 22, 'Shiro Sushi Lounge'),
(24, 1, 22, 'Mar na Boca'),
(25, 1, 53, 'Churrascaria Boi na Brasa'),
(26, 1, 44, 'Subway'),
(27, 1, 31, 'Bar Skina 10');

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `active`, `username`, `password`, `role`, `postal_code`, `address`, `number`, `complement`, `district`, `city`, `state`, `country`) VALUES
(1, 1, 'admin', '6ed87681bd0c5d9747c6217894bd7f6e4605069a', 'admin', '42700000', 'Rua Ibicaraí', 15, 'Ao lado do Colégio Equipe', 'Centro', 'Lauro de Freitas', 'BA', 'BRA'),
(2, 1, 'customer', 'eedc9daef4bb59b49201810d7e1de8c22bbbb04e', 'customer', '42700000', 'Rua Ibicaraí', 15, 'Ao lado do Colégio Equipe', 'Centro', 'Lauro de Freitas', 'BA', 'BRA'),
(3, 1, 'Matthew', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(4, 1, 'Erich', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(5, 1, 'Kitra', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(6, 1, 'Valentine', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(7, 1, 'Beau', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(8, 1, 'Aline', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(9, 1, 'Quinn', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(10, 1, 'Lee', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(11, 1, 'Ima', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(12, 1, 'Amal', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(13, 1, 'Barclay', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(14, 1, 'Harper', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(15, 1, 'Finn', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(16, 1, 'Ina', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(17, 1, 'Harrison', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(18, 1, 'Cheyenne', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(19, 1, 'Dieter', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(20, 1, 'Carolyn', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(21, 1, 'Joan', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(22, 1, 'Bevis', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(23, 1, 'Karleigh', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(24, 1, 'Autumn', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(25, 1, 'Raja', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(26, 1, 'Lee', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(27, 1, 'Yeo', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(28, 1, 'Merrill', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(29, 1, 'Mari', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(30, 1, 'Haley', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(31, 1, 'Dillon', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(32, 1, 'Deirdre', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(33, 1, 'Zoe', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(34, 1, 'Nasim', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(35, 1, 'Walter', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(36, 1, 'Baker', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(37, 1, 'Wayne', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(38, 1, 'Adria', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(39, 1, 'Frances', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(40, 1, 'Peter', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(41, 1, 'Piper', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(42, 1, 'Evelyn', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(43, 1, 'Willow', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(44, 1, 'Rhoda', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(45, 1, 'Alice', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(46, 1, 'Knox', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(47, 1, 'Yoko', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(48, 1, 'Wesley', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(49, 1, 'Denton', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(50, 1, 'Daquan', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(51, 1, 'Judah', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(52, 1, 'Margaret', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(53, 1, 'Kasimir', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(54, 1, 'Tucker', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(55, 1, 'Pascale', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(56, 1, 'Whilemina', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(57, 1, 'Zorita', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(58, 1, 'Dalton', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(59, 1, 'Daquan', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(60, 1, 'Kiona', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(61, 1, 'Cleo', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(62, 1, 'Lane', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(63, 1, 'Uma', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(64, 1, 'Carly', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(65, 1, 'Carter', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(66, 1, 'Hashim', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(67, 1, 'Orla', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(68, 1, 'Breanna', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(69, 1, 'Miranda', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(70, 1, 'Benedict', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(71, 1, 'Neil', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(72, 1, 'Ulysses', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(73, 1, 'Faith', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(74, 1, 'Mary', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(75, 1, 'Charles', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(76, 1, 'Sage', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(77, 1, 'Ezekiel', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(78, 1, 'Leah', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(79, 1, 'Upton', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(80, 1, 'Howard', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(81, 1, 'Callum', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(82, 1, 'Phyllis', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(83, 1, 'Sade', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(84, 1, 'Bruno', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(85, 1, 'Teegan', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(86, 1, 'Unity', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(87, 1, 'Eaton', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(88, 1, 'Elvis', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(89, 1, 'Zenia', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(90, 1, 'Calista', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(91, 1, 'Caleb', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(92, 1, 'Lara', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(93, 1, 'Halee', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(94, 1, 'Naomi', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(95, 1, 'Tatum', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(96, 1, 'Harriet', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(97, 1, 'Taylor', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(98, 1, 'Velma', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(99, 1, 'Adria', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(100, 1, 'Cyrus', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', ''),
(101, 1, 'Emerson', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'customer', '', '', 0, '', '', '', '', ''),
(102, 1, 'Karen', '0908b27b27868bf1a324202ac6a1919e238e8d11', 'admin', '', '', 0, '', '', '', '', '');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;