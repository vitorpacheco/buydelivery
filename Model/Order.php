<?php
App::uses('AppModel', 'Model');
/**
 * Order Model
 *
 * @property Restaurant $Restaurant
 * @property Plate $Plate
 */
class Order extends AppModel {

	const STATUS_PENDING = 1;
	const STATUS_PROCESSING = 2;
	const STATUS_APPROVED = 3;
	const STATUS_CANCELLED = 4;

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Plate' => array(
			'className' => 'Plate',
			'joinTable' => 'orders_plates',
			'foreignKey' => 'order_id',
			'associationForeignKey' => 'plate_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}