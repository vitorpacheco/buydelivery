<?php
App::uses('AppModel', 'Model');
/**
 * OrdersPlate Model
 *
 * @property Order $Order
 * @property Plate $Plate
 */
class OrdersPlate extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'order_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'plate_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'quantity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Order' => array(
			'className' => 'Order',
			'foreignKey' => 'order_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Plate' => array(
			'className' => 'Plate',
			'foreignKey' => 'plate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeSave() {
		$this->Order->recursive = 1;
		$newOrder = $this->Order->find('first', array('fields' => 'Order.*', 'conditions' => array('Order.id' => $this->data['OrdersPlate']['order_id'])));
		$totalSubtract = 0;
		$newTotal = 0;
		foreach ($newOrder['Plate'] as $plate) {
			if (($plate['OrdersPlate']['order_id'] == $this->data['OrdersPlate']['order_id'])
					&& ($plate['OrdersPlate']['plate_id'] == $this->data['OrdersPlate']['plate_id'])) {
				$totalSubtract = $plate['price'] * $plate['OrdersPlate']['quantity'];
				$newTotal = $plate['price'] * $this->data['OrdersPlate']['quantity'];
				break;
			}
		}
		if ($newTotal == 0) {
			$this->Plate->recursive = 0;
			$price = $this->Plate->find('first', array('fields' => 'Plate.*', 'conditions' => array('Plate.id' => $this->data['OrdersPlate']['plate_id'])));
			$newTotal = $this->data['OrdersPlate']['quantity'] * $price['Plate']['price'];
		}
		$newOrder['Order']['total'] -= $totalSubtract;
		$newOrder['Order']['total'] += $newTotal;
		if ($this->Order->save($newOrder['Order'])) {
			return true;
		} else {
			return false;
		}
		return true;
	}
}