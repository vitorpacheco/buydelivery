<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo __('BuyDelivery'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');

		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');

		echo $scripts_for_layout;
	?>
</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=253288944755531";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div id="container">
		<div id="content">
			<?php
			echo $this->Session->flash();
			echo $this->Session->flash('auth');

			echo $content_for_layout;

			echo $this->Html->link($this->Html->image('logo.png'), array('manage' => false, 'controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false));

			if (!empty($userData)) :?>
				<h4>Hello, <?php echo $this->Html->link($userData['username'], array('manage' => false, 'controller' => 'users', 'action' => 'view', $userData['id']))?>.</h4>
				<small><?php echo $this->Html->link(__('Logout'), array('manage' => false, 'controller' => 'users', 'action' => 'logout'));?></small>
			<?php endif;?>

			<div class="actions">
				<ul>
				<?php
				if (!empty($userData)) :?>
					<li><?php echo $this->Html->link(__('Edit Profile'), array('manage' => false, 'controller' => 'users', 'action' => 'edit')); ?></li>
					<li><?php echo $this->Html->link(__('My Orders'), array('manage' => false, 'controller' => 'orders', 'action' => 'index')); ?> </li>
				<?php else:?>
					<li><?php echo $this->Html->link(__('Login'), array('manage' => false, 'controller' => 'users', 'action' => 'login')); ?> </li>
					<li><?php echo $this->Html->link(__('Sign Up'), array('manage' => false, 'controller' => 'users', 'action' => 'add')); ?> </li>
				<?php endif;?>
				</ul>
				<hr />
				<h3>Navigation</h3>
				<ul>
					<li><?php echo $this->Html->link(__('Home'), array('manage' => false, 'controller' => 'pages', 'action' => 'display', 'home')); ?></li>
					<li><?php echo $this->Html->link(__('Plates'), array('manage' => false, 'controller' => 'plates', 'action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('Restaurants'), array('manage' => false, 'controller' => 'restaurants', 'action' => 'index')); ?> </li>

				</ul>
				<?php if (!empty($isAdmin) && $isAdmin === true):?>
				<hr />
				<h3>Management</h3>
				<ul>
					<li><?php echo $this->Html->link(__('Manage Users'), array('manage' => true, 'controller' => 'users', 'action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('Manage Orders'), array('manage' => true, 'controller' => 'orders', 'action' => 'index')); ?></li>
					<li><?php echo $this->Html->link(__('Manage Plates'), array('manage' => true, 'controller' => 'plates', 'action' => 'index', $userData['id'])); ?></li>
					<li><?php echo $this->Html->link(__('Manage Restaurants'), array('manage' => true, 'controller' => 'restaurants', 'action' => 'index')); ?> </li>

				</ul>
				<?php endif;?>
			</div>

		</div>
		<div id="footer">
			<hr />
			<center>
				<a href='https://pagseguro.uol.com.br' target='_blank'><img alt='Logotipos de meios de pagamento do PagSeguro' src='https://p.simg.uol.com.br/out/pagseguro/i/banners/pagamento/avista_estatico_550_70.gif' title='Este site aceita pagamentos com Oi Paggo, Bradesco, Itaú, Banco do Brasil, Banrisul, Banco HSBC, saldo em conta PagSeguro e boleto.' border='0'></a>
			</center>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>