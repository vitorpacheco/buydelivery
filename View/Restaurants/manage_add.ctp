<div class="restaurants form">
<?php echo $this->Form->create('Restaurant');?>
	<fieldset>
		<legend><?php echo __('Manage Add Restaurant'); ?></legend>
	<?php
		echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $userData['id']));
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>