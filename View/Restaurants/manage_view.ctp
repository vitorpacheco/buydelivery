<div class="restaurants view">
	<h2><?php  echo __('Restaurant');?>: <?php echo h($restaurant['Restaurant']['name']); ?></h2>
	<small>
		<?php echo __('Restaurant Code'); ?>: <?php echo h($restaurant['Restaurant']['id']); ?>
		<br />
		<?php echo $this->Html->link(__('Edit'), array('manage' => true, 'controller' => 'restaurants', 'action' => 'edit', $restaurant['Restaurant']['id']));?> |
		<?php echo $this->Form->postLink(__('Delete'), array('manage' => true, 'controller' => 'restaurants', 'action' => 'delete', $restaurant['Restaurant']['id']), null, __('Are you sure you want to delete # %s?', $restaurant['Restaurant']['id'])); ?>
	</small>
	<dl>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo $this->Util->active($restaurant['Restaurant']['active']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="related">
		<h3><?php echo __('Related Plates');?></h3>
		<?php if (!empty($restaurant['Plate'])):?>
		<table cellpadding = "0" cellspacing = "0">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<th><?php echo __('Title'); ?></th>
			<th><?php echo __('Active'); ?></th>
			<th><?php echo __('Price (R$)'); ?></th>
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
			$i = 0;
			foreach ($restaurant['Plate'] as $plate): ?>
			<tr>
				<td><?php echo $plate['id'];?></td>
				<td><?php echo $plate['title'];?></td>
				<td><?php echo $this->Util->active($plate['active']);?></td>
				<td><?php echo $plate['price'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'plates', 'action' => 'view', $plate['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'plates', 'action' => 'edit', $plate['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'plates', 'action' => 'delete', $plate['id']), null, __('Are you sure you want to delete # %s?', $plate['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
	</div>
</div>