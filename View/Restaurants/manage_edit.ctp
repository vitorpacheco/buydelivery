<div class="restaurants form">
<?php echo $this->Form->create('Restaurant');?>
	<fieldset>
		<legend><?php echo __('Manage Edit Restaurant'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('active');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
