<div class="restaurants view">
	<h2><?php  echo __('Restaurant');?>: <?php echo h($restaurant['Restaurant']['name']); ?></h2>
	<small><?php echo __('Restaurant Code'); ?>: <?php echo h($restaurant['Restaurant']['id']); ?></small>
	<div class="related">
		<h3><?php echo __('Related Plates');?></h3>
		<?php if (!empty($restaurant['Plate'])):?>
		<table cellpadding = "0" cellspacing = "0">
		<tr>
			<th><?php echo __('Title'); ?></th>
			<th><?php echo __('Price (R$)'); ?></th>
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($restaurant['Plate'] as $plate): ?>
			<tr>
				<td><?php echo $plate['title'];?></td>
				<td><?php echo $plate['price'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'plates', 'action' => 'view', $plate['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
	</div>
</div>