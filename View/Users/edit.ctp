<div class="users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('postal_code');
		echo $this->Form->input('address');
		echo $this->Form->input('number');
		echo $this->Form->input('complement');
		echo $this->Form->input('district');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('country', array('options' => Configure::read('User.countries')));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
