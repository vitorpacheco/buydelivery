<div class="users view">
	<h2><?php  echo __('User');?>: <?php echo h($user['User']['username']); ?></h2>
	<small>
		<?php  echo __('User code');?>: <?php echo h($user['User']['id']); ?>
		<br />
		<?php echo $this->Html->link(__('Edit'), array('manage' => true, 'controller' => 'users', 'action' => 'edit', $user['User']['id']));?> |
		<?php echo $this->Form->postLink(__('Delete'), array('manage' => true, 'controller' => 'users', 'action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
	</small>
	<dl>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo $this->Util->active($user['User']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Postal Code'); ?></dt>
		<dd>
			<?php echo h($user['User']['postal_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($user['User']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Number'); ?></dt>
		<dd>
			<?php echo h($user['User']['number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Complement'); ?></dt>
		<dd>
			<?php echo h($user['User']['complement']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('District'); ?></dt>
		<dd>
			<?php echo h($user['User']['district']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($user['User']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($user['User']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($user['User']['country']); ?>
			&nbsp;
		</dd>
	</dl>
</div>