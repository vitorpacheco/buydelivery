<div class="orders view">
	<h2><?php  echo __('Order');?>: <?php echo h($order['Order']['id']); ?></h2>
	<small>
		<?php echo $this->Html->link(__('Edit'), array('manage' => true, 'controller' => 'orders', 'action' => 'edit', $order['Order']['id']));?>
	</small>
	<dl>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($order['User']['username'], array('manage' => true, 'controller' => 'users', 'action' => 'view', $order['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo $this->Time->niceShort($order['Order']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo $this->Order->printStatus($order['Order']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total (R$)'); ?></dt>
		<dd>
			<?php echo h($order['Order']['total']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="related">
		<h3><?php echo __('Related Plates');?></h3>
		<?php if (!empty($order['Plate'])):?>
		<table cellpadding = "0" cellspacing = "0">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<th><?php echo __('Active'); ?></th>
			<th><?php echo __('Restaurant Id'); ?></th>
			<th><?php echo __('Title'); ?></th>
			<th><?php echo __('Price (R$)'); ?></th>
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
			$i = 0;
			foreach ($order['Plate'] as $plate): ?>
			<tr>
				<td><?php echo $plate['id'];?></td>
				<td><?php echo $this->Util->active($plate['active']);?></td>
				<td><?php echo $restaurants[$plate['restaurant_id']];?></td>
				<td><?php echo $plate['title'];?></td>
				<td><?php echo $plate['price'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('manage' => true, 'controller' => 'plates', 'action' => 'view', $plate['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('manage' => true, 'controller' => 'plates', 'action' => 'edit', $plate['id'])); ?>
					<?php echo $this->Form->postLink(__('Remove'), array('manage' => true, 'controller' => 'orders_plates', 'action' => 'delete', $plate['OrdersPlate']['id'], $order['Order']['id']), null, __('Are you sure you want to delete # %s?', $plate['OrdersPlate']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
	</div>
</div>