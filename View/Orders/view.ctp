<div class="orders view">
	<h2><?php  echo __('Order');?></h2>
	<dl>
		<dt><?php echo __('Order Number'); ?></dt>
		<dd>
			<?php echo h($order['Order']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($order['User']['username'], array('controller' => 'users', 'action' => 'view', $order['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($order['Order']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo $this->Order->printStatus($order['Order']['status']); ?>
			&nbsp;
		</dd>
	</dl>
	<div style="clear: both;"></div>
	<div class="related">
		<h3><?php echo __('Plates');?></h3>
		<?php if (!empty($order['Plate'])):?>
		<table cellpadding = "0" cellspacing = "0">
			<tr>
				<th><?php echo __('Restaurant'); ?></th>
				<th><?php echo __('Title'); ?></th>
				<th><?php echo __('Price (R$)'); ?></th>
				<th><?php echo __('Quantity'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
			<?php
				$i = 0;
				foreach ($order['Plate'] as $plate):?>
				<tr>
					<td><?php echo $restaurants[$plate['restaurant_id']];?></td>
					<td><?php echo $plate['title'];?></td>
					<td><?php echo $plate['price'];?></td>
					<td><?php echo $plate['OrdersPlate']['quantity'];?></td>
					<td class="actions">
						<?php echo $this->Html->link(__('View'), array('controller' => 'plates', 'action' => 'view', $plate['id'])); ?>
						<?php echo $this->Form->postLink(__('Remove'), array('controller' => 'orders_plates', 'action' => 'delete', $plate['OrdersPlate']['id'], $order['Order']['id'], false), null, __('Are you sure you want to delete # %s?', $plate['OrdersPlate']['id'])); ?>
					</td>
				</tr>
			<?php endforeach; ?>
				<tfoot>
					<tr>
						<th colspan="2"><?php echo __('Total (R$)'); ?></th>
						<th><?php echo h($order['Order']['total']); ?></th>
					</tr>
				</tfoot>
		</table>
	<?php endif; ?>
	</div>
	<?php
	echo $this->Form->create(null, array('target' => 'pagseguro', 'method' => 'post', 'url' => Configure::read('PagSeguro.url')));
	echo $this->Form->hidden('receiverEmail', array('value' => Configure::read('PagSeguro.receiverEmail'), 'name' => 'receiverEmail'));
	echo $this->Form->hidden('currency', array('value' => Configure::read('PagSeguro.currency'), 'name' => 'currency'));
	echo $this->Form->hidden('encoding', array('value' => Configure::read('PagSeguro.encoding'), 'name' => 'encoding'));
	$i = 0;
	foreach ($order['Plate'] as $plate):
		$i++;
		echo $this->Form->hidden('itemId' . $i, array('value' => $plate['id'], 'name' => 'itemId' . $i));
		echo $this->Form->hidden('itemDescription' . $i, array('value' => $plate['title'], 'name' => 'itemDescription' . $i));
		echo $this->Form->hidden('itemAmount' . $i, array('value' => $plate['price'], 'name' => 'itemAmount' . $i));
		echo $this->Form->hidden('itemQuantity' . $i, array('value' => $plate['OrdersPlate']['quantity'], 'name' => 'itemQuantity' . $i));
	endforeach;
	echo $this->Form->hidden('shippingType', array('value' => '2', 'name' => 'shippingType'));
	echo $this->Form->hidden('shippingAddressPostalCode', array('value' => $order['User']['postal_code'], 'name' => 'shippingAddressPostalCode'));
	echo $this->Form->hidden('shippingAddressStreet', array('value' => $order['User']['address'], 'name' => 'shippingAddressStreet'));
	echo $this->Form->hidden('shippingAddressNumber', array('value' => $order['User']['number'], 'name' => 'shippingAddressNumber'));
	echo $this->Form->hidden('shippingAddressComplement', array('value' => $order['User']['complement'], 'name' => 'shippingAddressComplement'));
	echo $this->Form->hidden('shippingAddressDistrict', array('value' => $order['User']['district'], 'name' => 'shippingAddressDistrict'));
	echo $this->Form->hidden('shippingAddressCity', array('value' => $order['User']['city'], 'name' => 'shippingAddressCity'));
	echo $this->Form->hidden('shippingAddressState', array('value' => $order['User']['state'], 'name' => 'shippingAddressState'));
	echo $this->Form->hidden('shippingAddressCountry', array('value' => $order['User']['country'], 'name' => 'shippingAddressCountry'));
	echo $this->Form->input(null, array('div' => false, 'label' => false, 'type' => 'image', 'name' => 'submit', 'src' => 'https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/184x42-pagar-cinza-assina.gif', 'alt' => 'Pague com PagSeguro'));
	echo $this->Form->end();
	?>
</div>
