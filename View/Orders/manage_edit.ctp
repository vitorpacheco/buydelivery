<div class="orders form">
<?php echo $this->Form->create('Order');?>
	<fieldset>
		<legend><?php echo __('Manage Edit Order'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id', array('type' => 'hidden'));
		echo $this->Order->inputStatus();
		echo $this->Form->input('total', array('label' => 'Total (R$)', 'readonly' => true));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>