<?php
App::uses('AppHelper', 'View/Helper');

/**
 * Order Helper
 *
 * @property HtmlHelper $Html
 * @property FormHelper $Form
 */
class OrderHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

	/**
	 * Print order status
	 *
	 * @param int $status
	 * @return string
	 */
	public function printStatus($status) {
		if ($status == Order::STATUS_PENDING) {
			return __('Pending');
		} else if ($status == Order::STATUS_PROCESSING) {
			return __('Processing');
		} else if ($status == Order::STATUS_APPROVED) {
			return __('Approved');
		} else if ($status == Order::STATUS_CANCELLED) {
			return __('Cancelled');
		}
	}

	public function inputStatus($status = null) {
		return $this->Form->input('status', array('value' => $status, 'options' => Configure::read('Order.status')));
	}

}