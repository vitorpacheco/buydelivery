<?php
App::uses('AppHelper', 'View/Helper');

/**
 * Order Helper
 *
 */
class UtilHelper extends AppHelper {
	public function active($value) {
		if ($value == true) {
			return __('Yes');
		} else {
			return __('No');
		}
	}
}