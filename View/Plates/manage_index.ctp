<div class="plates index">
	<h2><?php echo __('Plates');?></h2>
	<small><?php echo $this->Html->link(__('New'), array('manage' => true, 'controller' => 'plates', 'action' => 'add'));?></small>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('id');?></th>
		<th><?php echo $this->Paginator->sort('active');?></th>
		<th><?php echo $this->Paginator->sort('restaurant_id');?></th>
		<th><?php echo $this->Paginator->sort('title');?></th>
		<th><?php echo $this->Paginator->sort('price', 'Price (R$)');?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($plates as $plate): ?>
	<tr>
		<td><?php echo h($plate['Plate']['id']); ?>&nbsp;</td>
		<td><?php echo $this->Util->active($plate['Plate']['active']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($plate['Restaurant']['name'], array('controller' => 'restaurants', 'action' => 'view', $plate['Restaurant']['id'])); ?>
		</td>
		<td><?php echo h($plate['Plate']['title']); ?>&nbsp;</td>
		<td><?php echo h($plate['Plate']['price']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $plate['Plate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $plate['Plate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $plate['Plate']['id']), null, __('Are you sure you want to delete # %s?', $plate['Plate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>