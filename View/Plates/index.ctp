<script type="text/javascript">
	$(document).ready(function() {
		$(".buy-button").click(function(e) {
			var quantity = $("input[name='quantity']", $(this).parent().parent("form")).val();
			if (quantity <= 0) {
				alert("The quantity must be greater than zero.");
				e.preventDefault();
			}
		});
	});
</script>
<div class="plates index">
	<h2><?php echo __('Plates');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('restaurant_id');?></th>
		<th><?php echo $this->Paginator->sort('title');?></th>
		<th><?php echo $this->Paginator->sort('price');?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<tr>
		<?php echo $this->Form->create('Plate', array('inputDefaults' => array('label' => false)));?>
		<th><?php echo $this->Form->input('restaurant_id', array('empty' => 'Select'));?></th>
		<th><?php echo $this->Form->input('title');?></th>
		<th><?php echo $this->Form->input('price');?></th>
		<th class="actions"><?php echo $this->Form->end(__('Filter'));?></th>
	</tr>
	<?php
	foreach ($plates as $plate): ?>
	<tr>
		<td colspan="2">
			<h4><?php echo h($plate['Plate']['title']); ?></h4>
			<p><?php echo h($plate['Plate']['description']); ?></p>
			<small>Restaurant: <?php echo $this->Html->link($plate['Restaurant']['name'], array('controller' => 'restaurants', 'action' => 'view', $plate['Restaurant']['id'])); ?></small>
			<br />
			<small>Price: R$ <?php echo h($plate['Plate']['price']); ?></small>
			&nbsp;
		</td>
		<td class="actions">
			<?php
			echo $this->Form->create(null, array('type' => 'get', 'url' => array('controller' => 'orders_plates', 'action' => 'add_to_order')));
			echo $this->Form->input('plate_id', array('value' => $plate['Plate']['id'], 'type' => 'hidden', 'id' => 'PlateId' . $plate['Plate']['id']));
			echo $this->Form->input('quantity', array('id' => 'Quantity' . $plate['Plate']['id'], 'value' => 1));
			echo $this->Form->submit(__('Order'), array('class' => 'buy-button'));
			echo $this->Form->end();
			?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $plate['Plate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>