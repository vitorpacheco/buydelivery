<div class="plates form">
<?php echo $this->Form->create('Plate');?>
	<fieldset>
		<legend><?php echo __('Manage Add Plate'); ?></legend>
	<?php
		echo $this->Form->input('restaurant_id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>