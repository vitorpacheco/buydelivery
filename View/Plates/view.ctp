<div class="plates view">
	<h2>
		<div class="fb-like" data-href="http://delivery.local/plates/view/<?php echo h($plate['Plate']['id']);?>"
			data-send="true" data-layout="box_count" data-width="60"
			data-show-faces="false" data-font="arial"></div>
		<?php  echo __('Plate');?>: <?php echo h($plate['Plate']['title']); ?>
	</h2>
	<dl>
		<dt><?php echo __('Restaurant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($plate['Restaurant']['name'], array('controller' => 'restaurants', 'action' => 'view', $plate['Restaurant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($plate['Plate']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price (R$)'); ?></dt>
		<dd>
			<?php echo h($plate['Plate']['price']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="fb-comments" data-href="<?php echo env('HTTP_HOST') . Router::url(array('manage' => false, 'controller' => 'plates', 'action' => 'view', $plate['Plate']['id']));?>" data-num-posts="2" data-width="597"></div>
</div>