<div class="plates view">
	<h2>
		<div class="fb-like" data-href="http://delivery.local/plates/view/<?php echo h($plate['Plate']['id']);?>"
			data-send="true" data-layout="box_count" data-width="60"
			data-show-faces="false" data-action="recommend" data-font="arial"></div>
		<?php  echo __('Plate');?>: <?php echo h($plate['Plate']['title']); ?>
	</h2>
	<small>
		<?php echo $this->Html->link(__('Edit'), array('manage' => true, 'controller' => 'plates', 'action' => 'edit', $plate['Plate']['id']));?> |
		<?php echo $this->Form->postLink(__('Delete'), array('manage' => true, 'controller' => 'plates', 'action' => 'delete', $plate['Plate']['id']), null, __('Are you sure you want to delete # %s?', $plate['Plate']['id'])); ?>
	</small>
	<dl>
		<dt><?php echo __('Plate Code'); ?></dt>
		<dd>
			<?php echo h($plate['Plate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo $this->Util->active($plate['Plate']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Restaurant'); ?></dt>
		<dd>
			<?php echo $this->Html->link($plate['Restaurant']['name'], array('controller' => 'restaurants', 'action' => 'view', $plate['Restaurant']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($plate['Plate']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price (R$)'); ?></dt>
		<dd>
			<?php echo h($plate['Plate']['price']); ?>
			&nbsp;
		</dd>
	</dl>
	<div class="fb-comments" data-href="<?php echo env('HTTP_HOST') . Router::url(array('manage' => false, 'controller' => 'plates', 'action' => 'view', $plate['Plate']['id']));?>" data-num-posts="2" data-width="597"></div>
	<div class="related">
		<h3><?php echo __('Related Orders');?></h3>
		<?php if (!empty($plate['Order'])):?>
		<table cellpadding = "0" cellspacing = "0">
		<tr>
			<th><?php echo __('Id'); ?></th>
			<th><?php echo __('Created'); ?></th>
			<th><?php echo __('Status'); ?></th>
			<th><?php echo __('Total (R$)'); ?></th>
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
			$i = 0;
			foreach ($plate['Order'] as $order): ?>
			<tr>
				<td><?php echo $this->Html->link($order['id'], array('manage' => true, 'controller' => 'orders', 'action' => 'view', $order['id']));?></td>
				<td><?php echo $this->Time->niceShort($order['created']);?></td>
				<td><?php echo $this->Order->printStatus($order['status']);?></td>
				<td><?php echo $order['total'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'orders', 'action' => 'edit', $order['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), null, __('Are you sure you want to delete # %s?', $order['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>
	</div>
</div>