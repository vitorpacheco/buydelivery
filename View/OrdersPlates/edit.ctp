<div class="ordersPlates form">
<?php echo $this->Form->create('OrdersPlate');?>
	<fieldset>
		<legend><?php echo __('Edit Orders Plate'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('order_id');
		echo $this->Form->input('plate_id');
		echo $this->Form->input('quantity');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('OrdersPlate.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('OrdersPlate.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Orders Plates'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plates'), array('controller' => 'plates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plate'), array('controller' => 'plates', 'action' => 'add')); ?> </li>
	</ul>
</div>
