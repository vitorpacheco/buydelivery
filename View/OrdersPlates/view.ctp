<div class="ordersPlates view">
<h2><?php  echo __('Orders Plate');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ordersPlate['OrdersPlate']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ordersPlate['Order']['id'], array('controller' => 'orders', 'action' => 'view', $ordersPlate['Order']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plate'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ordersPlate['Plate']['title'], array('controller' => 'plates', 'action' => 'view', $ordersPlate['Plate']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($ordersPlate['OrdersPlate']['quantity']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Orders Plate'), array('action' => 'edit', $ordersPlate['OrdersPlate']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Orders Plate'), array('action' => 'delete', $ordersPlate['OrdersPlate']['id']), null, __('Are you sure you want to delete # %s?', $ordersPlate['OrdersPlate']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Orders Plates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Orders Plate'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plates'), array('controller' => 'plates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plate'), array('controller' => 'plates', 'action' => 'add')); ?> </li>
	</ul>
</div>
