<div class="ordersPlates index">
	<h2><?php echo __('Orders Plates');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('order_id');?></th>
			<th><?php echo $this->Paginator->sort('plate_id');?></th>
			<th><?php echo $this->Paginator->sort('quantity');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	foreach ($ordersPlates as $ordersPlate): ?>
	<tr>
		<td><?php echo h($ordersPlate['OrdersPlate']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($ordersPlate['Order']['id'], array('controller' => 'orders', 'action' => 'view', $ordersPlate['Order']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($ordersPlate['Plate']['title'], array('controller' => 'plates', 'action' => 'view', $ordersPlate['Plate']['id'])); ?>
		</td>
		<td><?php echo h($ordersPlate['OrdersPlate']['quantity']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ordersPlate['OrdersPlate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ordersPlate['OrdersPlate']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ordersPlate['OrdersPlate']['id']), null, __('Are you sure you want to delete # %s?', $ordersPlate['OrdersPlate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Orders Plate'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Orders'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Order'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plates'), array('controller' => 'plates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plate'), array('controller' => 'plates', 'action' => 'add')); ?> </li>
	</ul>
</div>
