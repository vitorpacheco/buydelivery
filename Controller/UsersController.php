<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add', 'logout');
	}

	public function isAuthorized($user) {
		if (!parent::isAuthorized($user)) {
			if (in_array($this->action, array('view', 'edit'))) {
				return true;
			}
			return false;
		}
		return true;
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (is_null($id)) {
			$id = $this->Auth->user('id');
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'edit'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
			unset($this->request->data['User']['password']);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Invalid username or password, try again.'));
			}
		}
	}

	public function logout() {
		$this->redirect($this->Auth->logout());
	}

/**
 * manage_index method
 *
 * @return void
 */
	public function manage_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * manage_view method
 *
 * @param string $id
 * @return void
 */
	public function manage_view($id = null) {
		$this->view($id);
	}

/**
 * manage_view method
 *
 * @param string $id
 * @return void
 */
	public function manage_add() {
		$this->add();
	}

/**
 * manage_edit method
 *
 * @param string $id
 * @return void
 */
	public function manage_edit($id = null) {
		$this->edit($id);
	}

/**
 * manage_edit method
 *
 * @param string $id
 * @return void
 */
	public function manage_delete($id = null) {
		$this->delete($id);
	}

}