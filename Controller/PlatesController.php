<?php
App::uses('AppController', 'Controller');
/**
 * Plates Controller
 *
 * @property Plate $Plate
 */
class PlatesController extends AppController {

	public $paginate = array(
		'conditions' => array('Plate.active' => 1),
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'view');
	}

	public function isAuthorized($user) {
		if (!parent::isAuthorized($user)) {
			return false;
		}
		return true;
	}

/**
 * index method
 *
 * @return void
 */
	public function index($user = null) {
		$this->Plate->recursive = 0;
		$conditions = array();
		if (is_null($user)) {
			$restaurants = $this->Plate->Restaurant->find('list');
			$conditions = $this->getPostConditions(array(
			'title' => 'LIKE',
			'price' => '<='
		));
		} else {
			$this->paginate['Plate'] = array('contain' => array('Restaurant'), 'conditions' => array('Restaurant.user_id' => $user));
			$restaurants = $this->Plate->Restaurant->find('list', array('conditions' => array('Restaurant.user_id' => $user)));
		}
		$plates = $this->paginate($conditions);
		$this->set(compact('plates', 'restaurants'));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Plate->id = $id;
		if (!$this->Plate->exists()) {
			throw new NotFoundException(__('Invalid plate'));
		}
		$this->set('plate', $this->Plate->read(null, $id));
	}

/**
 * manage_index method
 *
 * @return void
 */
	public function manage_index($user) {
		$this->index($user);
	}

/**
 * manage_add method
 *
 * @return void
 */
	public function manage_add() {
		if ($this->request->is('post')) {
			$this->Plate->create();
			if ($this->Plate->save($this->request->data)) {
				$this->Session->setFlash(__('The plate has been saved'));
				$this->redirect(array('action' => 'index', $this->Auth->user('id')));
			} else {
				$this->Session->setFlash(__('The plate could not be saved. Please, try again.'));
			}
		}
		$restaurants = $this->Plate->Restaurant->find('list', array('conditions' => array('Restaurant.user_id' => $this->Auth->user('id'))));
		$this->set(compact('restaurants'));
	}

/**
 * manage_view method
 *
 * @return void
 */
	public function manage_view($id = null) {
		$this->view($id);
	}

/**
 * manage_view method
 *
 * @return void
 */
	public function manage_edit($id = null) {
		$this->Plate->id = $id;
		if (!$this->Plate->exists()) {
			throw new NotFoundException(__('Invalid plate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Plate->save($this->request->data)) {
				$this->Session->setFlash(__('The plate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Plate->read(null, $id);
		}
		$restaurants = $this->Plate->Restaurant->find('list', array('conditions' => array('Restaurant.user_id' => $this->Auth->user('id'))));
		$this->set(compact('restaurants'));
	}

/**
 * manage_delete method
 *
 * @param string $id
 * @return void
 */
	public function manage_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Plate->id = $id;
		if (!$this->Plate->exists()) {
			throw new NotFoundException(__('Invalid plate'));
		}
		if ($this->Plate->delete()) {
			$this->Session->setFlash(__('Plate deleted'));
			$this->redirect(array('action' => 'index', $this->Auth->user('id')));
		}
		$this->Session->setFlash(__('Plate was not deleted'));
		$this->redirect(array('action' => 'index', $this->Auth->user('id')));
	}

}