<?php
App::uses('AppController', 'Controller');
/**
 * Orders Controller
 *
 * @property Order $Order
 */
class OrdersController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function isAuthorized($user) {
		if (!parent::isAuthorized($user)) {
			if (in_array($this->action, array('index', 'view', 'delete'))) {
				return true;
			}
			return false;
		}
		return true;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Order->recursive = 0;
		$this->set('orders', $this->paginate(array('user_id' => $this->Auth->user('id'))));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		$order = $this->Order->read(null, $id);
		$restaurants = $this->Order->Plate->Restaurant->find('list');
		$this->set(compact('order', 'restaurants'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->Order->delete()) {
			$this->Session->setFlash(__('Order deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Order was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * manage_index method
 *
 * @return void
 */
	public function manage_index() {
		$this->Order->recursive = 0;
		$this->set('orders', $this->paginate());
	}

/**
 * manage_edit method
 *
 * @param string $id
 * @return void
 */
	public function manage_edit($id = null) {
		$this->Order->id = $id;
		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid order'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('The order has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Order->read(null, $id);
		}
		$users = $this->Order->User->find('list');
		$plates = $this->Order->Plate->find('list');
		$this->set(compact('users', 'plates'));
	}

/**
 * manage_view method
 *
 * @param string $id
 * @return void
 */
	public function manage_view($id = null) {
		$this->view($id);
	}

/**
 * manage_delete method
 *
 * @param string $id
 * @return void
 */
	public function manage_delete($id = null) {
		$this->delete($id);
	}

}