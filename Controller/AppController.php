<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @property      AclComponent $Acl
 * @property      AuthComponent $Auth
 * @property      CookieComponent $Cookie
 * @property      EmailComponent $Email
 * @property      PaginatorComponent $Paginator
 * @property      RequestHandlerComponent $RequestHandler
 * @property      SecurityComponent $Security
 * @property      SessionComponent $Session
 */
class AppController extends Controller {

	public $paginate = array(
		'limit' => 15,
	);

	public $helpers = array(
		'Html',
		'Form',
		'Order',
		'Session',
		'Time',
		'Util',
	);

	public $components = array(
		'Session',
		'Auth' => array(
			'loginRedirect' => array('manage' => false, 'controller' => 'plates', 'action' => 'index'),
			'logoutRedirect' => array('manage' => false, 'controller' => 'pages', 'action' => 'display', 'home'),
			'authorize' => array('Controller'),
		)
	);

	public function isAuthorized($user) {
		if (isset($user['role']) && $user['role'] == 'admin') {
			return true;
		}
		return false;
	}

	public function beforeFilter() {
		$this->setUpAuth();
	}

	public function beforeRender() {
		$this->setUpUser();
	}

	protected function setUpAuth() {
		$this->Auth->authenticate = array(
			AuthComponent::ALL => array(
				'scope' => array(
					'User.active' => 1,
				)
			),
			'Form'
		);
		$this->Auth->loginAction = array('manage' => false, 'controller' => 'users', 'action' => 'login');
		$this->Auth->allow('display');
	}

	protected function setUpUser() {
		$isAdmin = false;
		$userData = $this->Auth->user();
		if ($userData) {
			if ($userData['role'] == 'admin') {
				$isAdmin = true;
			}
			$this->set(compact('userData', 'isAdmin'));
		}
	}

	protected function getPostConditions($options, $operator = 'AND') {
		$conditions = array();
		if ($this->request->is('post')) {
			foreach ($this->request->data[$this->modelClass] as $key => $field) {
				if (is_null($field) || empty($field)) {
					unset($this->request->data[$this->modelClass][$key]);
				}
			}
			$conditions = $this->postConditions(
				$this->request->data,
				$options,
				$operator
			);
		}
		return $conditions;
	}
}