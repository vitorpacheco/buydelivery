<?php
App::uses('AppController', 'Controller');
/**
 * OrdersPlates Controller
 *
 * @property OrdersPlate $OrdersPlate
 */
class OrdersPlatesController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function isAuthorized($user) {
		if (!parent::isAuthorized($user)) {
			if (in_array($this->action, array('add_to_order', 'getUserOrder', 'updateOrderTotal'))) {
				return true;
			}
			return false;
		}
		return true;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OrdersPlate->recursive = 0;
		$this->set('ordersPlates', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->OrdersPlate->id = $id;
		if (!$this->OrdersPlate->exists()) {
			throw new NotFoundException(__('Invalid orders plate'));
		}
		$this->set('ordersPlate', $this->OrdersPlate->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OrdersPlate->create();
			if ($this->OrdersPlate->save($this->request->data)) {
				$this->Session->setFlash(__('The orders plate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The orders plate could not be saved. Please, try again.'));
			}
		}
		$orders = $this->OrdersPlate->Order->find('list');
		$plates = $this->OrdersPlate->Plate->find('list');
		$this->set(compact('orders', 'plates'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->OrdersPlate->id = $id;
		if (!$this->OrdersPlate->exists()) {
			throw new NotFoundException(__('Invalid orders plate'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->OrdersPlate->save($this->request->data)) {
				$this->Session->setFlash(__('The orders plate has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The orders plate could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->OrdersPlate->read(null, $id);
		}
		$orders = $this->OrdersPlate->Order->find('list');
		$plates = $this->OrdersPlate->Plate->find('list');
		$this->set(compact('orders', 'plates'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null, $order = null, $manage = false) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->OrdersPlate->id = $id;
		if (!$this->OrdersPlate->exists()) {
			throw new NotFoundException(__('Invalid orders plate'));
		}
		if ($this->OrdersPlate->delete()) {
			$this->Session->setFlash(__('Orders plate deleted'));
			$this->redirect(array('manage' => $manage, 'controller' => 'orders', 'action' => 'view', $order));
		}
		$this->Session->setFlash(__('Orders plate was not deleted'));
		$this->redirect(array('manage' => $manage, 'controller' => 'orders', 'action' => 'view', $order));
	}

	public function add_to_order() {
		$user = $this->Auth->user('id');
		$plate = $this->request->query;
		$this->OrdersPlate->Plate->id = $plate['plate_id'];
		$plate['price'] = $this->OrdersPlate->Plate->field('price');
		if ($this->OrdersPlate->Plate->exists()) {
			$order = $this->getUserOrder($user);
			$OrdersPlate = $this->OrdersPlate->find('first', array(
				'conditions' => array('order_id' => $order['Order']['id'], 'plate_id' => $plate['plate_id']),
				'recursive' => -1,
				'contain' => array(),
			));
			if (!empty($OrdersPlate)) {
				$this->OrdersPlate->id = $OrdersPlate['OrdersPlate']['id'];
				if ($this->OrdersPlate->save(array('OrdersPlate' => array('quantity' => $plate['quantity'], 'plate_id' => $plate['plate_id'], 'order_id' => $order['Order']['id'])))) {
					$this->Session->setFlash(__('Quantity updated'));
					$this->redirect(array('controller' => 'plates', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Was not possible to add the plate, try again.'));
					$this->redirect(array('controller' => 'plates', 'action' => 'index'));
				}
			} else {
				$orderPlate['OrdersPlate'] = array(
					'order_id' => $order['Order']['id'],
					'plate_id' => $plate['plate_id'],
					'quantity' => $plate['quantity'],
				);
				$this->OrdersPlate->create();
				if ($this->OrdersPlate->save($orderPlate['OrdersPlate'], array('callbacks' => true))) {
					$this->Session->setFlash(__('Plate added to the order'));
					$this->redirect(array('controller' => 'plates', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Was not possible to add the plate, try again.'));
					$this->redirect(array('controller' => 'plates', 'action' => 'index'));
				}
			}
		}
	}

	public function getUserOrder($user) {
		$order = $this->OrdersPlate->Order->find('first', array(
			'conditions' => array('user_id' => $user, 'status' => Order::STATUS_PENDING),
			'recursive' => -1,
			'contain' => array(),
		));
		if (empty($order)) {
			$order['Order'] = array(
				'user_id' => $user,
				'status' => Order::STATUS_PENDING,
			);
			$this->OrdersPlate->Order->create();
			if ($this->OrdersPlate->Order->save($order)) {
				$order['Order']['id'] = $this->OrdersPlate->Order->id;
				return $order;
			} else {
				$this->Session->setFlash(__('Was not possible to add the plate, try again.'));
				$this->redirect(array('controller' => 'plates', 'action' => 'index'));
			}
		} else {
			return $order;
		}
	}

	public function manage_delete($id = null, $order) {
		$this->delete($id, $order, true);
	}
}