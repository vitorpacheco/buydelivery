<?php
App::uses('AppController', 'Controller');
/**
 * Restaurants Controller
 *
 * @property Restaurant $Restaurant
 */
class RestaurantsController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('index', 'view');
	}

	public function isAuthorized($user) {
		if (!parent::isAuthorized($user)) {
			return false;
		}
		return true;
	}

/**
 * index method
 *
 * @return void
 */
	public function index($user = null) {
		$this->Restaurant->recursive = 0;
		$conditions = array();
		if (is_null($user)) {
			$conditions = array_merge(
				$this->getPostConditions(array(
					'name' => 'LIKE',
				)), array('Restaurant.active' => 1));
		} else {
			$conditions = array('user_id' => $user);
		}

		$this->set('restaurants', $this->paginate($conditions));
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		$this->set('restaurant', $this->Restaurant->read(null, $id));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Restaurant->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

	}

/**
 * manage_index method
 *
 * @param string $id
 * @return void
 */
	public function manage_index($user = null) {
		if (is_null($user)) {
			$user = $this->Auth->user('id');
		}
		$this->index($user);
	}

/**
 * manage_view method
 *
 * @param string $id
 * @return void
 */
	public function manage_view($id = null) {
		$this->view($id);
	}

/**
 * manage_add method
 *
 * @return void
 */
	public function manage_add() {
		if ($this->request->is('post')) {
			$this->request->data['Restaurant']['user_id'] = $this->Auth->user('id');
			$this->Restaurant->create();
			if ($this->Restaurant->save($this->request->data)) {
				$this->Session->setFlash(__('The restaurant has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The restaurant could not be saved. Please, try again.'));
			}
		}
	}

/**
 * manage_edit method
 *
 * @param string $id
 * @return void
 */
	public function manage_edit($id = null) {
		$this->edit($id);
	}

/**
 * manage_delete method
 *
 * @param string $id
 * @return void
 */
	public function manage_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Restaurant->id = $id;
		if (!$this->Restaurant->exists()) {
			throw new NotFoundException(__('Invalid restaurant'));
		}
		if ($this->Restaurant->delete()) {
			$this->Session->setFlash(__('Restaurant deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Restaurant was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

}