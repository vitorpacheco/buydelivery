<?php
/* Plate Test cases generated on: 2012-03-03 00:35:04 : 1330745704*/
App::uses('Plate', 'Model');

/**
 * Plate Test Case
 *
 */
class PlateTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.plate', 'app.restaurant', 'app.order', 'app.orders_plate');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Plate = ClassRegistry::init('Plate');
	}

/**
 * testFind method
 *
 * @return void
 */
	public function testFind() {

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Plate);

		parent::tearDown();
	}

}
