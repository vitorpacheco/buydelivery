<?php
/* OrdersPlate Test cases generated on: 2012-03-03 00:35:04 : 1330745704*/
App::uses('OrdersPlate', 'Model');

/**
 * OrdersPlate Test Case
 *
 */
class OrdersPlateTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.orders_plate', 'app.order', 'app.restaurant', 'app.plate');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->OrdersPlate = ClassRegistry::init('OrdersPlate');
	}

/**
 * testFind method
 *
 * @return void
 */
	public function testFind() {

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OrdersPlate);

		parent::tearDown();
	}

}
