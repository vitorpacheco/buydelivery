<?php
/* Restaurant Test cases generated on: 2012-03-03 00:35:04 : 1330745704*/
App::uses('Restaurant', 'Model');

/**
 * Restaurant Test Case
 *
 */
class RestaurantTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.restaurant', 'app.order', 'app.plate', 'app.orders_plate');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Restaurant = ClassRegistry::init('Restaurant');
	}

/**
 * testFind method
 *
 * @return void
 */
	public function testFind() {

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Restaurant);

		parent::tearDown();
	}

}
