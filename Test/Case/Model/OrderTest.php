<?php
/* Order Test cases generated on: 2012-03-03 00:35:04 : 1330745704*/
App::uses('Order', 'Model');

/**
 * Order Test Case
 *
 */
class OrderTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.order', 'app.restaurant', 'app.plate', 'app.orders_plate');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Order = ClassRegistry::init('Order');
	}

/**
 * testFind method
 *
 * @return void
 */
	public function testFind() {

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Order);

		parent::tearDown();
	}

}
